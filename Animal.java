public class Animal{
    
    private String nume; 
    private int varsta;
    
    public Animal(String nume, int varsta){
        this.nume = nume; 
        this.varsta = varsta;
    }
    
    public void setNume(String nume){
        this.nume = nume; 
    }
    
    public String getNume(){
        return this.nume;
    }
    
    public void setVarsta(int varsta){
        this.varsta = varsta;
    }
    
    public int getVarsta(){
        return this.varsta;
    }
}