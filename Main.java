import java.util.Scanner;

public class Main{
    public static void main (String [] args){
        
        NrAnimale nr = new NrAnimale();
        Thread t = new Thread(nr);
        t.start();
        
        Adapost adapost;
        adapost = Adapost.getInstance();

        Scanner sc = new Scanner(System.in);
        String line = "";
        
        while (true){
            line = sc.nextLine();
            String [] cmd = line.split("\\s+");
            switch(cmd[0]){
                case "ADAUGA_ANIMAL":
                        String tip = cmd[1];
                        String nume = cmd[2];
                        int varsta = Integer.parseInt(cmd[3]);
                    try{
                        if (tip.equals("domestic")){
                            boolean inf = Boolean.parseBoolean(cmd[4]);
                            Domestic aD = new Domestic(nume, varsta, inf);
                            adapost.adaugaAnimal(aD);
                            System.out.println("A fost adaugat un animal domestic");
                        } else if(tip.equals("salbatic")){
                            int info = Integer.parseInt(cmd[4]);
                            Salbatic aS = new Salbatic(nume, varsta, info);
                            adapost.adaugaAnimal(aS);
                            System.out.println("A fost adaugat un animal salbatic");
                        }
                    }catch(NumeDejaExistentException e){
                        System.out.println(e.getMessage());
                    }   
                break;
                case "AFIS":
                    adapost.afiseazaAnimale();
                break;
                
                case "AFIS_DOMESTICE":
                    adapost.afiseazaAnimaleDomestice();
                break;
                
                case "AFIS_SORT":
                    adapost.afiseazaSortat();
                break;
                
                case "ADOPTA": 
                    String num = cmd[1];
                    adapost.adoptaAnimal(num);
                break;
                
                case "EXIT":
                    System.exit(0);
            }
        }
    }
}