public class Domestic extends Animal{
    
    private boolean deInterior;
    
    public Domestic (String nume, int varsta, boolean deInterior){
        super(nume, varsta);
        this.deInterior = deInterior;
    }
    
    public void setDeInterior(boolean deInterior){
        this.deInterior = deInterior;
    }
    
    public boolean isDeInterior(){
        return this.deInterior;
    }
    
    @Override
    public String toString(){
        return "Animalul domestic " + this.getNume() + " are varsta de " + 
                this.getVarsta() + " si este animal de interior " + this.isDeInterior() + "\n";
    }
    
    @Override
    public boolean equals(Object o){
        if (o == null){
            return false; 
        }
        
        if (!(o instanceof Animal)){
            return false;
        }
        Animal a = (Animal)o;
        if(a.getNume().equals(this.getNume())){
            return true;
        }else {
            return false;
        }
    }
}