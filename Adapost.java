import java.util.*;

public class Adapost{
    
    private ArrayList<Animal> animale = new ArrayList<Animal>();
    
    private static Adapost instance = null;
    
    private Adapost(){
    }
    
    public static Adapost getInstance(){
        if(instance == null){
            instance = new Adapost();
        }
        return instance;
    }
    
    public void adaugaAnimal(Animal a) throws NumeDejaExistentException{
           if(animale.contains(a)){
               throw new NumeDejaExistentException();
            }else {
             animale.add(a);
            }
        } 

    public void afiseazaAnimale(){
        for(Animal a : animale){
            System.out.println(a);
        }
    }
    
    public void afiseazaAnimaleDomestice(){
        for(int i = 0; i<animale.size(); i++){
            if(animale.get(i) instanceof Domestic){
                System.out.println(animale.get(i));
            }
        }
    }
    
    public void afiseazaSortat(){
        animale.sort(new SortareNume());   
        System.out.println(animale);
    }
    
    public int getIndex(Animal a){
        return animale.indexOf(a);
    }
    
    public void adoptaAnimal(String num){
        for(int i = 0; i<animale.size(); i++){
            Animal an = animale.get(i);
            if(an.getNume().equals(num)){
                int idx = this.getIndex(an);
                animale.remove(idx);
                System.out.println("A fost adoptat un animal " + an.getNume());
            }
        }
    }
 
    public int getAnimale(){
        return animale.size();
    }
}