public class Salbatic extends Animal{
    
    private int grPericol;
    
    public Salbatic (String nume, int varsta, int grPericol){
        super(nume, varsta);
        if (grPericol>=0 && grPericol <=5){
            this.grPericol = grPericol;
        }else {
            System.out.println("gradul de pericol trebuie sa fie cuprins intre 0 si 5");
        }
    }
    
    public void setGrPericol(int grPericol){
         if (grPericol>=0 && grPericol <=5){
            this.grPericol = grPericol;
        }else {
            System.out.println("gradul de pericol trebuie sa fie cuprins intre 0 si 5");
        }
    }
    
    public int getGrPericol(){
        return this.grPericol;
    }
    
    @Override
    public String toString(){
        return "Animalul salbatic " + this.getNume() + " are varsta de " + 
                this.getVarsta() + " si gradul de pericol " + this.getGrPericol() + "\n";
    }
    
    @Override
    public boolean equals(Object o){
        if (o == null){
            return false; 
        }
        
        if (!(o instanceof Animal)){
            return false;
        }
        Animal a = (Animal) o;
        if(a.getNume().equals(this.getNume())){
            return true;
        }else {
            return false;
        }
    }
}