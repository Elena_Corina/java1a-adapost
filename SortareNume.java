import java.util.*;

public class SortareNume implements Comparator<Animal>{
    
    @Override
    public int compare(Animal a1, Animal a2){
        return a1.getNume().compareToIgnoreCase(a2.getNume());
    }
}